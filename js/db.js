var location_id,location_e,location_h;
//app section start
var appsettings = new Lawnchair({
        adapter: "dom",
        name: "settings"
    }, function(appsettings) {
    });
    
    appsettings.exists('first', function(available){
        if(!available){
         
            // create an object
            var run = {
                key: 'first',value:'1'
            };
            var fruit = {
                key:'fruit',value:fruits
            };
            var city = {
                key:'city',value:cities
            }
            var vege = {
                key:'veg',value:veges
            };
            var lang = {
            	key:'lang',value:'en'
            };
            // save it
            appsettings.save(run);
            appsettings.save(fruit);
            appsettings.save(city);
            appsettings.save(vege);
            appsettings.save(lang)
        }        
        
    });
function populate_city(){
	appsettings.get('city', function(cityd) {
            var lt = cityd.value;
            //var run = JSON.stringify(me.value);
    for (var i =0;i<= lt.length-1; i++) {
        var op = '<div class="grid"><div class="col2">'+lt[i].city_e+'</div><div class="col2"><span><input type="checkbox" id="loc'+i+'" style="display:block;"  value='+city_array[i].location_e+' ></span></div></div>'
        $('#list_loc').append(op);
        }; 
    });
}
function toggle_lang(){
	appsettings.get('lang',function(data){
		if (data.value=='en') {
			var lnh = {
				key:'lang',value:'hin'
			}
			appsettings.save(lnh);
			clrall();
			list_init_h();
			$("button.lang").html('See in English');
			$('center#fruit_list > .afFakeSelect').html('आपना फल चुनें..')
			$('center#veg_cen > .afFakeSelect').html('अपनी सब्जी चुनें..');
			$("#set_lang").html('हिंदी');
			$.ui.launch();
		}if(data.value=='hin'){
			var lne = {
				key:'lang',value:'en'
			}
            $("button.lang").html('हिंदी में देखें');
			$('center#fruit_list > .afFakeSelect').html('Choose your fruit..');
			$('center#veg_cen > .afFakeSelect').html('Choose your vegetable..');
			appsettings.save(lne);
			$("#set_lang").html('English');
			clrall();
			list_init_e();
			$.ui.launch();
		}
	});
}

function clrall(){
	$("select#fruit_l").empty();
	$("select#veg_l").empty();
}


function list_init_e () {
    appsettings.get('fruit',function(data){
        var fr = data.value;
        $('select#fruit_l').append('Choose a fruit..');
        for (var i =0;i<= fr.length-1; i++) {
        $('select#fruit_l').append('<option>'+fr[i].fruit_e+'</option>');
        };
    });
    appsettings.get('veg',function(dat) {
    var vg = dat.value;
    $('select#veg_l').append('Choose a vegetable..');
    for (var i =0;i<= vg.length-1; i++) {
    $('select#veg_l').append('<option>'+vg[i].veg_e+'</option>');
    };
    });

}
function list_init_h () {
	appsettings.get('fruit',function(data){
        var fr = data.value;
        $('select#fruit_l').append('Choose a fruit..');
        for (var i =0;i<= fr.length-1; i++) {
        $('select#fruit_l').append('<option>'+fr[i].fruit_h+'</option>');
        };
    });
  	appsettings.get('veg',function(data) {
    	var vg = data.value;
    	$('select#veg_l').append('Choose a vegetable..');
    	for (var i =0;i<= vg.length-1; i++) {
    	$('select#veg_l').append('<option>'+vg[i].veg_h+'</option>');
    	};
    });
}
var init = function(){
	appsettings.get('lang',function(data){
		var lan = data.value;
		if (lan=='en') {
			list_init_e();
		}if (lan=='hin') {
			list_init_h();
		};
		toggle_lang();
	});
}
//app section end